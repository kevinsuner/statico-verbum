<!--
SPDX-License-Identifier: GPL-3.0-only
Copyright (C) 2024 Kevin Suñer <ksuner@pm.me>
-->

{{ define "header" }}
<!DOCTYPE html>
<html lang="{{ .meta.lang }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="{{ .meta.robots }}">
        <meta name="keywords" content="{{ .meta.keywords }}">
        <meta name="description" content="{{ .meta.description }}">
        <meta name="author" content="{{ .meta.author }}">
        <meta property="og:type" content="{{ .meta.type }}">
        <meta property="og:url" content="{{ .meta.base_url }}index.html">
        <meta property="og:title" content="{{ .meta.title }}">
        <meta property="og:description" content="{{ .meta.description }}">
        {{ if eq .meta.type "article" }}
        <meta property="article:section" content="{{ .meta.section }}">
        <meta property="article:published_time" content="{{ .meta.pub_date }}">
        <meta property="article:modified_time" content="{{ .meta.mod_date }}">
        {{ end }}
        <link rel="canonical" href="{{ .meta.base_url }}index.html">
        <link rel="icon" href="{{ .meta.base_url }}images/favicon.ico">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ .meta.base_url }}images/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ .meta.base_url }}images/favicon-16x16.png">
        <link rel="apple-touch-icon" href="{{ .meta.base_url }}images/apple_touch_icon.png">
        <link href="{{ .meta.base_url }}bootstrap/bootstrap.css" rel="stylesheet">
        <link href="{{ .meta.base_url }}css/style.css" rel="stylesheet">
        {{ if .meta.prism }}
        <link href="{{ .meta.base_url }}prism/prism.css" rel="stylesheet">
        {{ end }}
        <title>{{ .meta.title }}</title>
    </head>
    <body>
{{ end }}

