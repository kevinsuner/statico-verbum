/*
 * SPDX-License-Identifier: GPL-3.0-only
 * Copyright (C) 2024 Kevin Suñer <ksuner@pm.me>
 */

/*
 * Adds or removes the 'hidden' class from an article element.
 * @param {Object} articles - The array containing all the 'article' tags.
 * @param {number} max - The maximum number of articles to display.
 * @param {number} page - The current page.
 */
function showPage(articles, max, page) {
    const startIndex = page * max;
    const endIndex = startIndex + max;
    articles.forEach((article, index) => {
        article.classList.toggle('hidden', index < startIndex || index >= endIndex);
    });
};

/*
 * Creates the button(s) to go through the paginated articles.
 * @param {Object} content - The root element containing all the articles.
 * @param {Object} articles - The array containing all the 'article' tags.
 * @param {number} max - The maximum number of articles to display.
 * @param {number} page - The current page.
 */
function createPagination(content, articles, max, page) {
    const totalPages = Math.ceil(articles.length / max);
    
    const paginationContainer = document.createElement('ul');
    paginationContainer.classList.add('pagination', 'pagination-sm', 'justify-content-center', 'py-2');
    
    const pagination = document.body.appendChild(paginationContainer);

    for (let i = 0; i < totalPages; i++) {
        const li = document.createElement('li');
        li.classList.add('page-link', 'link-dark')
        li.textContent = i + 1;

        li.addEventListener('click', function() {
            page = i;
            showPage(articles, max, page);
        });

        content.appendChild(paginationContainer);
        pagination.appendChild(li);
    }
};

document.addEventListener('DOMContentLoaded', function() {
    const max = 5;

    const content = document.querySelector('.content');
    const articles = Array.from(content.getElementsByTagName('article'));

    let page = 0;
    createPagination(content, articles, max, page);
    showPage(articles, max, page);

    const contentMobile = document.querySelector('.content-mobile');
    const articlesMobile = Array.from(contentMobile.getElementsByTagName('article'));

    let pageMobile = 0;
    createPagination(contentMobile, articlesMobile, max, pageMobile);
    showPage(articlesMobile, max, pageMobile);
});

